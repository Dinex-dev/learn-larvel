@extends('larvelexample.layout.root')
@section('extra-style')
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection

@section('content')
<div class="container head">
  <div>
    <h1>What is Laravel ?</h1>
    <p>Laravel is a free, open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller (MVC) architectural pattern.
    </p>
    <a target="_blank" class="btn btn-secondary" href="https://laravel.com/" role="button"><i class="fas fa-laravel"> Laravel</i>
    </a>
  </div>

  <img src="https://dinex-dev.github.io/images/profile.jpg" class="img-thumbnail mx-auto" alt="">
</div>
@endsection